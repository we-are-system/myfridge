var gulp = require("gulp");
var sass = require("gulp-sass");
var autoprefixer = require("gulp-autoprefixer")
var plumber = require("gulp-plumber");
var sourcemaps = require("gulp-sourcemaps");
var concat = require("gulp-concat");
var livereload = require("gulp-livereload");
var mainBowerFiles = require("gulp-main-bower-files");
var filter = require("gulp-filter");
var uglify = require('gulp-uglify');

gulp.task("html",function(){
  gulp.src('./*.html')
    .pipe(plumber())
    .pipe(livereload())
});

gulp.task("js",function(){
  gulp.src("./src/js/*.js")
    .pipe(plumber())
    .pipe(uglify())
    .pipe(gulp.dest("./dist/js/"))
    .pipe(livereload())
});

gulp.task("sass", function(){
  gulp.src("./src/scss/*.scss")
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(sass({
      includePaths: ["./bower_components/bootstrap-sass/assets/stylesheets"], 
      outputStyle: 'compressed'
    }))
    .pipe(autoprefixer({
      browsers: ['last 2 versions', "ie 9"]
    }))
    .pipe(sourcemaps.write("./maps/"))
    .pipe(gulp.dest("./dist/css/"))
    .pipe(livereload());
})

gulp.task("bs-copy-fonts", function(){
  gulp.src("./bower_components/bootstrap-sass/assets/fonts/**/*").pipe(gulp.dest("./dist/fonts/"))
})

gulp.task("jsconcat", function(){
  var filterJS = filter("**/*.js")
  return gulp.src("./bower.json")
    .pipe(mainBowerFiles({
      overrides: {
        "vue-strap": {
          main: "dist/vue-strap.js"
        }, 
        "vuetable": {
          main: "dist/vue-table.js"
        }
      }
    }))
    .pipe(filterJS)
    .pipe(concat("_vendor.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./dist/js/"))
    .pipe(livereload());
})

gulp.task("watch", function(){
  livereload.listen();
  gulp.watch("*.html", ["html"]);
  gulp.watch("./src/scss/*.scss", ["sass"]);
  gulp.watch("./src/js/*.js", ["js"]);
})

