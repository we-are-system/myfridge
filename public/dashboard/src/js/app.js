(function(){

Vue.config.debug = false;
Vue.config.silent = true;
Vue.config.devtools = false;

//new Vue({
//    el: "",
//    components: {},
//    data: {},
//    methods: {}
//})

Vue.mixin({
    methods: {
        toggleAlert: function (alert) {
            var arg = alert.split('.');
            this.showAlert[arg[0]][arg[1]] = !this.showAlert[arg[0]][arg[1]];
        }
    }
});

Vue.http.options.emulateHTTP = true;
Vue.http.options.emulateJSON = true;

var vueFridgeList = new Vue({
    el: "#fridge-list",
    components: {
        alert: VueStrap.alert,
        modal: VueStrap.modal,
        datepicker: VueStrap.datepicker,
        sidebar: VueStrap.aside,
        spinner: VueStrap.spinner
    },
    data: {
        showAlert: {
            error: {
                auth: false,
                genericApi: false,
                suggestApi: false
            },
            success: {
                update: false,
                delete: false
            }
        },
        showModal: {
            view: false,
            edit: false,
            confirmDelete: false
        },
        showAside: false,
        asideData: [],
        suggestData: [],
        columns: [
            {
                name: 'pretty.created_at',
                title: '登録日時',
                sortField: 'created_at'
            },
            {
                name: 'name',
                title: '名称',
                sortField: 'name'
            },
            {
                name: 'pretty.expiration_date',
                title: '期限',
                sortField: 'expiration'
            },
            {
                name: 'memo',
                title: 'メモ',
                callback: 'shrinkMemo'
            },
            '__actions'
        ],
        itemActions: [
            {
                name: 'view-item',
                label: '',
                icon: 'glyphicon glyphicon-zoom-in',
                class: 'btn btn-info'
            },
            {
                name: 'edit-item',
                label: '',
                icon: 'glyphicon glyphicon-pencil',
                class: 'btn btn-warning'
            },
            {
                name: 'delete-item',
                label: '',
                icon: 'glyphicon glyphicon-remove',
                class: 'btn btn-danger'
            }
        ],
        deleteCandidate: {
            id: -1,
            name: ""
        },
        datatoedit: {
            id: -1,
            data: {
                name: "",
                expiration: "",
                memo: ""
            }
        },
        searchFor: '',
        sortOrder: {
            field: 'name',
            direction: 'asc'
        },
        perPage: 10
    },
    watch: {
        'perPage': function (val, oldVal) {
            this.$broadcast('vuetable:refresh')
        }
    },
    methods: {
        setModalDataAndShow: function (data) {
            this.$set('datatoview', data);
            this.showModal.view = true;
        },
        newItem: function () {
            vueItemSelector.newItem();
        },
        updateItem: function () {
            this.$refs.savingSpinner.show();
            this.$http.patch('/api/fridges/' + this.datatoedit.id, this.datatoedit)
                .then(function (response) {
                    this.$refs.savingSpinner.hide();
                    this.showModal.edit = false;
                    this.toggleAlert('success.update');
                    this.reload();
                }, function (data, response) {
                    console.log(data);
                    console.log(response);
                    this.$refs.savingSpinner.hide();
                    this.showModal.edit = false;
                    this.toggleAlert('error.genericApi');
                })
        },
        deleteItem: function () {
            this.$refs.deletingSpinner.show();
            this.$http.delete('/api/fridges/' + this.deleteCandidate.id)
                .then(function (response) {
                    if (response.data.status == "success") {
                        this.toggleAlert("success.delete");
                    } else {
                        this.toggleAlert("error.genericApi");
                    }
                    this.$refs.deletingSpinner.hide();
                    this.reload();
                    this.showModal.confirmDelete = false;
                }, function (data, response) {
                    this.toggleAlert("error.genericApi");
                    console.log(data);
                    console.log(response);
                    this.$refs.deletingSpinner.hide();
                    this.showModal.confirmDelete = false;
                })
            this.deleteCandidate.id = -1;
        },
        shrinkMemo: function (memo) {
            if (memo.length > 10) {
                return memo.slice(0, 10) + "..."
            } else {
                return memo
            }
        },
        reload: function () {
            this.$http({
                url: '/api/fridges?to=5',
                method: 'GET'
            }).then(function (response) {
                if (response.data.status == "success") {
                    this.asideData = response.data.fridges;
                } else {
                    this.toggleAlert("error.genericApi");
                }
            }, function (response) {
                this.toggleAlert("error.genericApi");
            });
            this.$http({
                url: '/api/suggests',
                method: 'GET'
            }).then(function (response) {
                if (response.data.status == "success") {
                    this.suggestData = response.data.suggests;
                } else {
                    this.toggleAlert("error.suggestApi");
                }
            }, function (response) {
                this.toggleAlert("error.suggestApi");
            });
            this.$broadcast('vuetable:reload');
        }
    },
    ready: function () {
        var self = this;
        this.$broadcast('vuetable-pagination:set-options', {
            wrapperClass: 'pagination',
            icons: {
                first: '',
                prev: '',
                next: '',
                last: ''
            },
            activeClass: 'active',
            linkClass: 'btn btn-default',
            pageClass: 'btn btn-default'
        })
        this.$http({
            url: '/api/fridges?to=5',
            method: 'GET'
        }).then(function (response) {
            if (response.data.status == "success") {
                this.asideData = response.data.fridges;
                setTimeout(function () {
                    self.showAside = true;
                }, 500);
                setTimeout(function () {
                    self.showAside = false;
                }, 2500);
            } else {
                this.toggleAlert("error.genericApi");
            }
        }, function (response) {
            this.toggleAlert("error.genericApi");
        });
        this.$http({
            url: '/api/suggests',
            method: 'GET'
        }).then(function (response) {
            if (response.data.status == "success") {
                this.suggestData = response.data.suggests;
            } else {
                this.toggleAlert("error.suggestApi");
            }
        }, function (response) {
            this.toggleAlert("error.suggestApi");
        });

    },
    events: {
        'vuetable:action': function (action, data) {
            switch (action) {
            case 'view-item':
                this.setModalDataAndShow(data);
                break;
            case 'edit-item':
                console.log(data);
                this.datatoedit.id = data.id;
                this.$set('datatoedit.data', {
                    name: data.name,
                    expiration: data.expiration,
                    memo: data.memo
                });
                this.showModal.edit = true;
                break;
            case 'delete-item':
                this.deleteCandidate.id = data.id;
                this.deleteCandidate.name = data.name;
                this.showModal.confirmDelete = true;
                break;
            default:
                console.log(action, data);
            }
        },
        'vuetable:loading': function () {
            this.$refs.loadingSpinner.show();
        },
        'vuetable:load-success': function (response) {
            this.$refs.loadingSpinner.hide();
        },
        'vuetable:load-error': function (response) {
            this.$refs.loadingSpinner.hide();
            this.toggleAlert("error.genericApi");
            if (response.status === 401) {
                this.toggleAlert("error.auth");
            }
            console.log('Load Error: ', response)
        }
    }
})

var vueItemSelector = new Vue({
    el: "#item-selector",
    components: {
        alert: VueStrap.alert,
        modal: VueStrap.modal,
        datepicker: VueStrap.datepicker,
        spinner: VueStrap.spinner
    },
    data: {
        showAlert: {
            error: {
                genericApi: false,
                addItem: false
            },
            success: {
                addItem: false
            }
        },
        showDetail: false,
        categories: [],
        ingredients: [],
        candidateIngredient: {
            name: "name",
            date: "",
            memo: ""
        },
        searchFor: ""
    },
    methods: {
        select: function (event) {
            var el = $(event.target);
            el.siblings().removeClass('active');
            el.addClass('active');
            var id = el.data('mf-selector-id');
            var spinner = setTimeout(function () {
                this.$refs.ingredientSpinner.show()
            }, 3000);
            if (id === 0) {
                var url = '/api/ingredients';
            } else {
                var url = '/api/categories/' + id + '/ingredients';
            }
            this.$http({
                url: url,
                method: 'GET'
            }).then(function (response) {
                if (response.data.status == "success") {
                    this.ingredients = response.data.ingredients;
                } else {
                    this.toggleAlert("error.genericApi");
                }
                clearTimeout(spinner);
                this.$refs.ingredientSpinner.hide();
            }, function (response) {
                this.toggleAlert("error.genericApi");
            });
        },
        newItem: function () {
            this.candidateIngredient.name = "";
            this.candidateIngredient.date = "";
            this.showDetail = true;
        },
        openDetail: function (event) {
            var el = $(event.target);
            var id;
            if (el.data('mf-ingredient-id') !== undefined) {
                id = el.data('mf-ingredient-id');
            } else {
                id = el.parent().data('mf-ingredient-id');
            }
            var ingredient = $.grep(this.ingredients, function (e) {
                return e.id === id;
            });
            this.candidateIngredient.name = ingredient[0].name;
            this.candidateIngredient.date = ingredient[0].expire_date;
            this.showDetail = true;
        },
        saveCandidate: function () {
            this.$refs.savingSpinner.show();
            this.$http.post('/api/fridges', {
                fridge: this.candidateIngredient
            }).then(function (data, status, response) {
                if (data.data.status === "success") {
                    this.toggleAlert("success.addItem");
                    vueFridgeList.reload();
                } else {
                    this.toggleAlert("error.addItem");
                }
            }, function (data, status, response) {
                this.toggleAlert("error.addItem");;
            });
            this.$refs.savingSpinner.hide();
            this.showDetail = false;
        },
        img: function (name) {
            return '/dashboard/images/' + name + '.png'
        }
    },
    ready: function () {
        this.$http({
            url: '/api/categories',
            method: 'GET'
        }).then(function (response) {
            if (response.data.status == "success") {
                this.categories = response.data.categories;
                this.categories.unshift({
                    id: 0,
                    name: "すべて"
                });
            } else {
                this.toggleAlert("error.genericApi");
            }
        }, function (response) {
            this.toggleAlert("error.genericApi");
        });

    }
})

})();
