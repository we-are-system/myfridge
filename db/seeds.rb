@category = Category.new
@category.name = 'パン・米・麺類'
@category.save

@category = Category.new
@category.name = '魚・肉'
@category.save

@category = Category.new
@category.name = '加工食品・惣菜'
@category.save

@category = Category.new
@category.name = '菓子・デザート'
@category.save

@category = Category.new
@category.name = '卵・牛乳・乳製品'
@category.save

@category = Category.new
@category.name = '果物・野菜・きのこ'
@category.save

@category = Category.new
@category.name = '冷凍食品'
@category.save

@category = Category.new
@category.name = '調味料・その他'
@category.save

@ingredient = Ingredient.new
@ingredient.category_id = 1
@ingredient.name = 'パン'
@ingredient.expiration = '4'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 1
@ingredient.name = '米'
@ingredient.expiration = '60'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 1
@ingredient.name = 'うどん（乾燥）'
@ingredient.expiration = '365'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 1
@ingredient.name = 'そば（乾燥）'
@ingredient.expiration = '365'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 1
@ingredient.name = 'ラーメン（乾燥）'
@ingredient.expiration = '105'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 1
@ingredient.name = 'ひやむぎ（乾燥）'
@ingredient.expiration = '730'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 1
@ingredient.name = 'うどん（冷凍）'
@ingredient.expiration = '365'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 1
@ingredient.name = 'そば（冷凍）'
@ingredient.expiration = '365'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 1
@ingredient.name = 'ラーメン（冷凍）'
@ingredient.expiration = '365'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 1
@ingredient.name = 'ひやむぎ（冷凍）'
@ingredient.expiration = '365'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 1
@ingredient.name = 'うどん'
@ingredient.expiration = '12'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 1
@ingredient.name = 'そば'
@ingredient.expiration = '10'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 1
@ingredient.name = 'ラーメン'
@ingredient.expiration = '15'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 1
@ingredient.name = 'ひやむぎ'
@ingredient.expiration = '5'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '牛肉（こま切れ）'
@ingredient.expiration = '2'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '豚肉（こま切れ）'
@ingredient.expiration = '2'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '牛肉（薄切り）'
@ingredient.expiration = '3'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '豚肉（薄切り）'
@ingredient.expiration = '3'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '牛肉（冷凍）'
@ingredient.expiration = '21'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '豚肉（冷凍）'
@ingredient.expiration = '21'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '鶏肉（冷凍）'
@ingredient.expiration = '21'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '鶏むね肉'
@ingredient.expiration = '2'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '鶏もも肉'
@ingredient.expiration = '2'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '鶏ささみ肉'
@ingredient.expiration = '1'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '鶏手羽肉'
@ingredient.expiration = '2'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '合いびき肉（冷凍）'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '牛ひき肉（冷凍）'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '豚ひき肉（冷凍）'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '鶏ひき肉（冷凍）'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '合いびき肉'
@ingredient.expiration = '1'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '牛ひき肉'
@ingredient.expiration = '1'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '豚ひき肉'
@ingredient.expiration = '1'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '鶏ひき肉'
@ingredient.expiration = '1'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = 'レバー'
@ingredient.expiration = '1'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = 'レバー（冷凍）'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '魚'
@ingredient.expiration = '4'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 2
@ingredient.name = '魚（冷凍）'
@ingredient.expiration = '21'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 3
@ingredient.name = '納豆'
@ingredient.expiration = '10'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 3
@ingredient.name = '豆腐'
@ingredient.expiration = '10'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 3
@ingredient.name = '厚揚げ'
@ingredient.expiration = '6'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 3
@ingredient.name = 'こんにゃく'
@ingredient.expiration = '60'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 3
@ingredient.name = 'キムチ'
@ingredient.expiration = '7'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 3
@ingredient.name = 'レトルト食品'
@ingredient.expiration = '365'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 3
@ingredient.name = 'カップ麺'
@ingredient.expiration = '150'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 3
@ingredient.name = '魚缶'
@ingredient.expiration = '1095'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 3
@ingredient.name = 'ツナ缶'
@ingredient.expiration = '1095'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 3
@ingredient.name = '果物缶'
@ingredient.expiration = '1095'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 3
@ingredient.name = 'ソース缶'
@ingredient.expiration = '730'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 3
@ingredient.name = 'スープ缶'
@ingredient.expiration = '730'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 3
@ingredient.name = 'コーン缶'
@ingredient.expiration = '730'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 3
@ingredient.name = 'メンマ'
@ingredient.expiration = '550'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 3
@ingredient.name = 'かまぼこ'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 3
@ingredient.name = 'ちくわ'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 4
@ingredient.name = 'ポテトチップス'
@ingredient.expiration = '120'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 4
@ingredient.name = 'チョコレート'
@ingredient.expiration = '120'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 4
@ingredient.name = 'せんべい'
@ingredient.expiration = '120'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 4
@ingredient.name = '和菓子'
@ingredient.expiration = '3'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 4
@ingredient.name = 'アイスクリーム'
@ingredient.expiration = '730'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 4
@ingredient.name = 'ケーキ'
@ingredient.expiration = '1'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = '卵'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = '牛乳'
@ingredient.expiration = '10'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = 'カッテージチーズ'
@ingredient.expiration = '7'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = 'カマンベールチーズ'
@ingredient.expiration = '7'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = 'ゴーダチーズ'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = 'エダムチーズ'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = 'ブルーチーズ'
@ingredient.expiration = '10'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = 'モッツァレラチーズ'
@ingredient.expiration = '2'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = '6ｐチーズ'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = '粉チーズ'
@ingredient.expiration = '30'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = 'スモークチーズ'
@ingredient.expiration = '5'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = 'スライスチーズ'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = 'ピザ用チーズ'
@ingredient.expiration = '30'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = '生クリーム'
@ingredient.expiration = '3'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = 'バター'
@ingredient.expiration = '180'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = 'マーガリン'
@ingredient.expiration = '300'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 5
@ingredient.name = 'ヨーグルト'
@ingredient.expiration = '15'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'トマト'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'きゅうり'
@ingredient.expiration = '3'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'ねぎ'
@ingredient.expiration = '7'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'たまねぎ'
@ingredient.expiration = '60'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'なす'
@ingredient.expiration = '3'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'にんじん'
@ingredient.expiration = '7'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'じゃがいも'
@ingredient.expiration = '30'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = '大根'
@ingredient.expiration = '10'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'キャベツ'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'レタス'
@ingredient.expiration = '4'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'ほうれん草'
@ingredient.expiration = '7'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'ピーマン'
@ingredient.expiration = '7'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'かぼちゃ'
@ingredient.expiration = '90'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'ごぼう'
@ingredient.expiration = '10'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = '白菜'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'もやし'
@ingredient.expiration = '7'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'バナナ'
@ingredient.expiration = '7'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'りんご'
@ingredient.expiration = '30'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'イチゴ'
@ingredient.expiration = '4'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'みかん'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'ぶどう'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'キウイフルーツ'
@ingredient.expiration = '14'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'しいたけ'
@ingredient.expiration = '5'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'エリンギ'
@ingredient.expiration = '3'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'マイタケ'
@ingredient.expiration = '6'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'えのき'
@ingredient.expiration = '3'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'しめじ'
@ingredient.expiration = '6'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 6
@ingredient.name = 'えだまめ'
@ingredient.expiration = '3'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 7
@ingredient.name = '冷凍食品'
@ingredient.expiration = '365'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 8
@ingredient.name = '砂糖'
@ingredient.expiration = '9999'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 8
@ingredient.name = 'グラニュー糖'
@ingredient.expiration = '9999'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 8
@ingredient.name = '醤油'
@ingredient.expiration = '365'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 8
@ingredient.name = 'みりん'
@ingredient.expiration = '365'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 8
@ingredient.name = '料理酒'
@ingredient.expiration = '550'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 8
@ingredient.name = 'マヨネーズ'
@ingredient.expiration = '180'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 8
@ingredient.name = 'ケチャップ'
@ingredient.expiration = '60'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 8
@ingredient.name = 'サラダ油'
@ingredient.expiration = '730'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 8
@ingredient.name = 'ウスターソース'
@ingredient.expiration = '730'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 8
@ingredient.name = 'オイスターソース'
@ingredient.expiration = '730'
@ingredient.save

@ingredient = Ingredient.new
@ingredient.category_id = 8
@ingredient.name = '味噌'
@ingredient.expiration = '180'
@ingredient.save

@recipe = Recipe.new
@recipe.name = '豚キムチ'
@recipe.ingredients = ["豚肉（こま切れ）", "キムチ", "ねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '豚キムチ'
@recipe.ingredients = ["豚肉（薄切り）", "キムチ", "ねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '肉じゃが'
@recipe.ingredients = ["豚肉（こま切れ）", "じゃがいも", "たまねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '生姜焼き'
@recipe.ingredients = ["豚肉（こま切れ）"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '生姜焼き'
@recipe.ingredients = ["豚肉（薄切り）"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '切り干し大根'
@recipe.ingredients = ["豚肉（こま切れ）", "大根", "しいたけ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '豚肉とキャベツのごま味噌炒め'
@recipe.ingredients = ["豚肉（こま切れ）", "キャベツ", "ピーマン"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '豚肉とキャベツのごま味噌炒め'
@recipe.ingredients = ["豚肉（薄切り）", "キャベツ", "ピーマン"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '豚汁'
@recipe.ingredients = ["豚肉（こま切れ）", "大根", "ねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ポークソテーきのこソース'
@recipe.ingredients = ["豚肉（こま切れ）", "大根", "しめじ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ポークソテーきのこソース'
@recipe.ingredients = ["豚肉（こま切れ）", "大根", "マイタケ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '豚肉とほうれん草の炒め物'
@recipe.ingredients = ["豚肉（こま切れ）", "ほうれん草"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '豚肉とほうれん草の炒め物'
@recipe.ingredients = ["豚肉（薄切り）", "ほうれん草"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'カレー'
@recipe.ingredients = ["豚肉（こま切れ）", "じゃがいも", "にんじん", "たまねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '肉うどん'
@recipe.ingredients = ["牛肉（こま切れ）", "うどん"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '肉うどん'
@recipe.ingredients = ["牛肉（こま切れ）", "うどん（冷凍）"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '肉うどん'
@recipe.ingredients = ["牛肉（こま切れ）", "うどん（乾燥）"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ビーフストロガノフ'
@recipe.ingredients = ["牛肉（こま切れ）", "しめじ", "たまねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ビーフストロガノフ'
@recipe.ingredients = ["牛肉（薄切り）", "しめじ", "たまねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '牛肉とほうれん草の炒め物'
@recipe.ingredients = ["牛肉（こま切れ）", "ほうれん草"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '牛肉とほうれん草の炒め物'
@recipe.ingredients = ["牛肉（薄切り）", "ほうれん草"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '牛筋のトマト煮込み'
@recipe.ingredients = ["牛肉（こま切れ）", "トマト"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '牛筋のトマト煮込み'
@recipe.ingredients = ["牛肉（薄切り）", "トマト"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '肉じゃが'
@recipe.ingredients = ["牛肉（こま切れ）", "じゃがいも", "たまねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '牛肉と玉ねぎのすき焼き風炒め'
@recipe.ingredients = ["牛肉（こま切れ）", "たまねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '牛筋肉の煮込み'
@recipe.ingredients = ["牛肉（こま切れ）", "ねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '青椒肉絲'
@recipe.ingredients = ["牛肉（こま切れ）", "ピーマン"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '青椒肉絲'
@recipe.ingredients = ["牛肉（薄切り）", "ピーマン"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'カレー'
@recipe.ingredients = ["牛肉（こま切れ）", "じゃがいも", "にんじん", "たまねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '鶏肉と彩り野菜のオイスター炒め'
@recipe.ingredients = ["鶏むね肉", "ピーマン", "たまねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '鶏肉と彩り野菜のオイスター炒め'
@recipe.ingredients = ["鶏もも肉", "ピーマン", "たまねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '鶏肉のトマト煮'
@recipe.ingredients = ["鶏むね肉", "トマト"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '鶏肉のトマト煮'
@recipe.ingredients = ["鶏もも肉", "トマト"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '手羽先の唐揚げ'
@recipe.ingredients = ["鶏手羽肉"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'チキンチーズカツ'
@recipe.ingredients = ["鶏むね肉", "スライスチーズ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'チキンチーズカツ'
@recipe.ingredients = ["鶏もも肉", "スライスチーズ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'チキンチーズカツ'
@recipe.ingredients = ["鶏むね肉", "ピザ用チーズ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'チキンチーズカツ'
@recipe.ingredients = ["鶏もも肉", "ピザ用チーズ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '照り焼きチキン'
@recipe.ingredients = ["鶏むね肉"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '照り焼きチキン'
@recipe.ingredients = ["鶏もも肉"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ささみフライ'
@recipe.ingredients = ["鶏ささみ肉"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'チキンマリネ'
@recipe.ingredients = ["鶏むね肉", "玉ねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'チキンマリネ'
@recipe.ingredients = ["鶏もも肉", "玉ねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '棒棒鶏'
@recipe.ingredients = ["鶏むね肉", "きゅうり", "ラーメン"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '棒棒鶏'
@recipe.ingredients = ["鶏むね肉", "きゅうり", "ラーメン（冷凍）"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '棒棒鶏'
@recipe.ingredients = ["鶏むね肉", "きゅうり", "ラーメン（乾燥）"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '親子丼'
@recipe.ingredients = ["鶏むね肉", "玉ねぎ", "卵"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '親子丼'
@recipe.ingredients = ["鶏もも肉", "玉ねぎ", "卵"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '鶏肉の水炊き'
@recipe.ingredients = ["鶏むね肉", "白菜"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '鶏肉の水炊き'
@recipe.ingredients = ["鶏もも肉", "白菜"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '鶏そぼろ丼'
@recipe.ingredients = ["鶏ひき肉（冷凍）", "米"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '鶏そぼろ丼'
@recipe.ingredients = ["鶏ひき肉", "米"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '鶏肉のそぼろ煮'
@recipe.ingredients = ["鶏ひき肉（冷凍）"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '鶏肉のそぼろ煮'
@recipe.ingredients = ["鶏ひき肉"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ひき肉あんかけ豆腐'
@recipe.ingredients = ["鶏ひき肉（冷凍）", "豆腐"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ひき肉あんかけ豆腐'
@recipe.ingredients = ["鶏ひき肉", "豆腐"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '熟成しょうゆ豚そぼろ'
@recipe.ingredients = ["豚ひき肉"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '熟成しょうゆ豚そぼろ'
@recipe.ingredients = ["豚ひき肉（冷凍）"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ぎょうざ'
@recipe.ingredients = ["豚ひき肉", "キャベツ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ぎょうざ'
@recipe.ingredients = ["豚ひき肉（冷凍）", "キャベツ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ぶた茄子丼'
@recipe.ingredients = ["豚ひき肉", "なす"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ぶた茄子丼'
@recipe.ingredients = ["豚ひき肉（冷凍）", "なす"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '野菜豚挽肉カレー'
@recipe.ingredients = ["豚ひき肉", "かぼちゃ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '野菜豚挽肉カレー'
@recipe.ingredients = ["豚ひき肉（冷凍）", "かぼちゃ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ハンバーグ'
@recipe.ingredients = ["豚ひき肉", "たまねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ハンバーグ'
@recipe.ingredients = ["豚ひき肉（冷凍）", "たまねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ハンバーグ'
@recipe.ingredients = ["牛ひき肉", "たまねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ハンバーグ'
@recipe.ingredients = ["牛ひき肉（冷凍）", "たまねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ハンバーグ'
@recipe.ingredients = ["合いびき肉", "たまねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ハンバーグ'
@recipe.ingredients = ["合いびき肉（冷凍）", "たまねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '牛しぐれ煮'
@recipe.ingredients = ["牛ひき肉"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '牛しぐれ煮'
@recipe.ingredients = ["牛ひき肉（冷凍）"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '牛肉コロッケ'
@recipe.ingredients = ["牛ひき肉", "じゃがいも"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '牛肉コロッケ'
@recipe.ingredients = ["牛ひき肉（冷凍）", "じゃがいも"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'メンチカツ'
@recipe.ingredients = ["合いびき肉", "じゃがいも"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'メンチカツ'
@recipe.ingredients = ["合いびき肉（冷凍）", "じゃがいも"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ピーマンの肉詰め'
@recipe.ingredients = ["豚ひき肉", "ピーマン"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ピーマンの肉詰め'
@recipe.ingredients = ["豚ひき肉（冷凍）", "ピーマン"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ピーマンの肉詰め'
@recipe.ingredients = ["牛ひき肉", "ピーマン"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ピーマンの肉詰め'
@recipe.ingredients = ["牛ひき肉（冷凍）", "ピーマン"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ピーマンの肉詰め'
@recipe.ingredients = ["合いびき肉", "ピーマン"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ピーマンの肉詰め'
@recipe.ingredients = ["合いびき肉（冷凍）", "ピーマン"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '麻婆豆腐'
@recipe.ingredients = ["豚ひき肉", "豆腐"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '麻婆豆腐'
@recipe.ingredients = ["豚ひき肉（冷凍）", "豆腐"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '麻婆豆腐'
@recipe.ingredients = ["牛ひき肉", "豆腐"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '麻婆豆腐'
@recipe.ingredients = ["牛ひき肉（冷凍）", "豆腐"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '麻婆豆腐'
@recipe.ingredients = ["合いびき肉", "豆腐"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '麻婆豆腐'
@recipe.ingredients = ["合いびき肉（冷凍）", "豆腐"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'レバーケチャップ炒め'
@recipe.ingredients = ["レバー"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'レバーケチャップ炒め'
@recipe.ingredients = ["レバー（冷凍）"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'とうふの照り焼き丼'
@recipe.ingredients = ["豆腐", "米"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '湯豆腐'
@recipe.ingredients = ["豆腐"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'とうふのチーズフライ'
@recipe.ingredients = ["豆腐", "スライスチーズ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'とうふのチーズフライ'
@recipe.ingredients = ["豆腐", "ピザ用チーズ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '豆腐ステーキ'
@recipe.ingredients = ["豆腐", "ねぎ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '豆腐ハンバーグ'
@recipe.ingredients = ["豆腐", "卵"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '厚揚げ大根煮'
@recipe.ingredients = ["厚揚げ", "大根"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '味噌汁'
@recipe.ingredients = ["厚揚げ", "なす"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '味噌汁'
@recipe.ingredients = ["厚揚げ", "豆腐"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ちくわの磯辺揚げ'
@recipe.ingredients = ["ちくわ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'たまご雑炊'
@recipe.ingredients = ["米", "卵"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'チャーハン'
@recipe.ingredients = ["米", "卵"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'きのこごはん'
@recipe.ingredients = ["米", "しめじ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'きのこごはん'
@recipe.ingredients = ["米", "えのき"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'オムライス'
@recipe.ingredients = ["米", "卵", "鶏むね肉"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'オムライス'
@recipe.ingredients = ["米", "卵", "鶏もも肉"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'オムライス'
@recipe.ingredients = ["米", "卵", "鶏肉（冷凍）"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'チキンライス'
@recipe.ingredients = ["米", "鶏むね肉"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'チキンライス'
@recipe.ingredients = ["米", "鶏もも肉"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'チキンライス'
@recipe.ingredients = ["米", "鶏肉（冷凍）"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ドリア'
@recipe.ingredients = ["米", "ピザ用チーズ", "牛乳"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ごろごろなすのドリア'
@recipe.ingredients = ["米", "ピザ用チーズ", "牛乳", "なす"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ミートドリア'
@recipe.ingredients = ["米", "ピザ用チーズ", "牛乳", "ソース缶"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '牛肉のトマトハヤシドリア'
@recipe.ingredients = ["米", "ピザ用チーズ", "牛乳", "牛肉（こま切れ）", "トマト"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '牛肉のトマトハヤシドリア'
@recipe.ingredients = ["米", "ピザ用チーズ", "牛乳", "牛肉（薄切り）", "トマト"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '牛肉のトマトハヤシドリア'
@recipe.ingredients = ["米", "ピザ用チーズ", "牛乳", "牛肉（冷凍）", "トマト"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'ほうれん草のドリア'
@recipe.ingredients = ["米", "ピザ用チーズ", "牛乳", "ほうれん草"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'トマト麻婆ドリア'
@recipe.ingredients = ["米", "ピザ用チーズ", "牛乳", "ほうれん草", "トマト"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'とうふドリア'
@recipe.ingredients = ["米", "ピザ用チーズ", "牛乳", "豆腐"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'かぼちゃのスープドリア'
@recipe.ingredients = ["米", "ピザ用チーズ", "牛乳", "かぼちゃ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'チキンドリア'
@recipe.ingredients = ["米", "ピザ用チーズ", "牛乳", "鶏むね肉"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'チキンドリア'
@recipe.ingredients = ["米", "ピザ用チーズ", "牛乳", "鶏もも肉"]
@recipe.save

@recipe = Recipe.new
@recipe.name = 'チキンドリア'
@recipe.ingredients = ["米", "ピザ用チーズ", "牛乳", "鶏肉（冷凍）"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '白菜としめじのクリーム煮'
@recipe.ingredients = ["白菜", "しめじ"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '白菜のトマト煮'
@recipe.ingredients = ["白菜", "トマト"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '厚揚げ白菜大根煮'
@recipe.ingredients = ["厚揚げ", "白菜", "大根"]
@recipe.save

@recipe = Recipe.new
@recipe.name = '白菜のうま煮'
@recipe.ingredients = ["厚揚げ", "白菜"]
@recipe.save

