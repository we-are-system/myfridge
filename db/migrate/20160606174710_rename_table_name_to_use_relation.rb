class RenameTableNameToUseRelation < ActiveRecord::Migration
  def change
    change_table :ingredients do |t|
      t.rename :category, :category_id
    end

    change_table :fridges do |t|
      t.rename :user, :user_id
    end
  end
end
