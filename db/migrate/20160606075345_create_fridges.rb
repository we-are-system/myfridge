class CreateFridges < ActiveRecord::Migration
  def change
    create_table :fridges do |t|
      t.integer :user
      t.string :name
      t.date :expiration
      t.string :memo

      t.timestamps null: false
    end
  end
end
