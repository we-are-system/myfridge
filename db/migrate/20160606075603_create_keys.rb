class CreateKeys < ActiveRecord::Migration
  def change
    create_table :keys do |t|
      t.string :access_token
      t.datetime :expired_at
      t.integer :user_id
      t.datetime :created_at
      t.datetime :updated_at
      t.boolean :active

      t.timestamps null: false
    end
  end
end
