class FixFridgeExpiration < ActiveRecord::Migration
  def up
    change_column :fridges, :expiration, :date, null: false
  end
  def down
    change_column :fridges, :user_id, :integer, null: true
    change_column :fridges, :expiration, :date, null: true
  end
end
