class ChangeColumnToFridge < ActiveRecord::Migration
  def up
    change_column :fridges, :user_id, :integer, null: false
    change_column :fridges, :expiration, :string, null: false
  end
  def down
    change_column :fridges, :user_id, :integer, null: true
    change_column :fridges, :expiration, :string, null: true
  end
end
