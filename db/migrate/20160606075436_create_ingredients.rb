class CreateIngredients < ActiveRecord::Migration
  def change
    create_table :ingredients do |t|
      t.integer :category
      t.string :name
      t.integer :expiration

      t.timestamps null: false
    end
  end
end
