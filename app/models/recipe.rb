class Recipe
  include Mongoid::Document
  field :name
  field :ingredients
end
