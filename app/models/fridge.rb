class Fridge < ActiveRecord::Base
    belongs_to :user
    scope :date_between, -> from, to {
      if from.present? && to.present?
        where(expiration: from..to)
      elsif from.present?
        where('expiration >= ?', from)
      elsif to.present?
        where('expiration <= ?', to)
      end
    }
end
