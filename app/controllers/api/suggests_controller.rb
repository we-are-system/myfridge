class Api::SuggestsController < ApplicationController
  before_action do
    unless session[:user_id]
      render json: {status: "error", detail: "unauthorized"}, status: :unauthorized
    end
  end  

  def get
    user = current_user
    ingredients = user.fridges.map{|e| e.name}
    suggests = Recipe.where(ingredients: {'$not' => {'$elemMatch' => {'$nin' => ingredients}}})
    render json: {status: 'success', suggests: suggests}
  end

end
