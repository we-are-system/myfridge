class Api::RecipesController < ApplicationController
  def index
    recipes = Recipe.all
    render json: {status: 'success', recipes: recipes}
  end
  def show
    begin
      recipe = Recipe.find(id: params[:id])
    rescue
      render json: {status: 'error', detail: 'not found'} and return
    end
    render json: {status: 'success', recipe: recipe}
  end
end
