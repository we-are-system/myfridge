class Api::FridgesController < ApplicationController
  before_action do
    unless session[:user_id]
      render json: {status: "error", detail: "unauthorized"}, status: :unauthorized
    end
  end

  def index
    user = current_user
    fridges = user.fridges
    if date = params[:to]
        to = (Date.today + params[:to].to_i)
        fridges = fridges.date_between(nil, to)
    end

    order = params[:sort].presence || 'id'
    order.gsub!('|', ' ')

    if page = params[:page]
      per = params[:per_page].presence || '10'
      fridges = fridges.order(order).page(page).per(per)
      pagination = {
        total: user.fridges.count, 
        per_page: per, 
        current_page: page, 
        last_page: fridges.total_pages, 
        from: (fridges.offset_value + 1), 
        to: (fridges.offset_value + fridges.length)
      }
    else
      fridges = fridges.order(order)
      pagination = {}
    end
    fridges = fridges.map{|i| i.attributes}

    fridges.each do |fridge|
      add_pretty fridge
    end

    render json: {status: "success", fridges: fridges, pagination: pagination}
  end

  def create
    fridge = Fridge.new do |f|
      f.user_id = current_user.id
      f.name = params[:fridge][:name]
      f.memo = params[:fridge][:memo]
      unless params[:fridge][:date] == ""
        f.expiration = params[:fridge][:date]
      else
        render json: {status: "error", detail: "date can't be null"} and return
      end
    end
    if fridge.save
      render json: {status: "success"} and return
    else
      render json: {status: "error", detail: "db save error"} and return
    end
  end

  def show
    user = current_user
    if fridge = user.fridges.find_by_id(params[:id])
      fridge = fridge.attributes
      add_pretty fridge
      render json: {status: "success", fridge: fridge} and return
    else
      render json: {status: "error", detail: "record not found"} and return
    end
  end

  def update
    user = current_user
    if fridge = user.fridges.find_by_id(params[:id])
      if fridge.update_attributes(params[:data].permit(:name, :expiration, :memo))
        render json: {status: "success", fridge: fridge} and return
      else
        render json: {status: "error", detail: "record save error"} and return
      end
    else
      render json: {status: "error", detail: "record not found"} and return
    end
  end

  def destroy
    if f = current_user.fridges.find_by_id(params[:id])
      f.destroy
      render json: {status: "success", fridge: f} and return
    else
      render json: {status: "error", detail: "record not found"} and return
    end
  end

  private
  def add_pretty(hash)
      hash.store('pretty', {
        created_at: hash["created_at"].strftime("%Y/%m/%d %k:%M"), 
        expiration_date: hash["expiration"].strftime("%Y/%m/%d"), 
        expiration: (hash["expiration"] - Date.today).to_i 
      })
  end
  

end
