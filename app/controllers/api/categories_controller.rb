class Api::CategoriesController < ApplicationController
  def index
    categories = Category.all
    render json: {status: "success", categories: categories}
  end

  def show
    category = Category.find(params[:id])
    render json: {status: "success", category: category}
  end
end
