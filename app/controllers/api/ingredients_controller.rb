class Api::IngredientsController < ApplicationController
  def index
    if params[:category_id]
      category = Category.find(params[:category_id])
      ingredients = category.ingredients
    else
      ingredients = Ingredient.all
    end

    # convert to hash
    ingredients = ingredients.map{|i| i.attributes}

    ingredients.each do |ingredient|
      expire_date = (Date.today + ingredient['expiration']).strftime("%Y/%m/%d")
      ingredient.store('expire_date', expire_date)
    end

    render json: {status: "success", ingredients: ingredients}
  end

  def show
    ingredient = Ingredient.find(params[:id])
    render json: {status: "success", ingredient: ingredient}
  end
end
