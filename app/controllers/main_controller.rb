class MainController < ApplicationController
  def judge
    if session[:user_id] == nil
      redirect_to '/login'
    else
      redirect_to '/dashboard'
    end
  end
  
  def profile
    errors = {"Name can't be blank" => "名前の欄を入力してください",
              "Email can't be blank" => "メールアドレスの欄を入力して下さい" ,
              "Email is invalid" => "メールアドレスの形が不正です",
              "Password can't be blank" => "パスワードの欄を入力してください",
              "Password is too short (minimum is 6 characters)" => "パスワードは6文字以上で入力して下さい",
              "This Email address is already signed up" => "このメールアドレスはすでに登録されています",
              "Passwords are mismatch" => "２つのパスワードが異なります", 
              "Profile is updated" => "ユーザ情報が更新されました"}
    if flash[:notice] != nil
      flash[:notice].each do |msg|
        msg.sub!(msg, errors[msg])
      end
    end
    if flash[:notice] == nil
      flash[:notice] = ""
    end
    @user = User.find_by(:id => session[:user_id])
  rescue => e
    flash[:notice].clear
    redirect_to '/profile'
  end

  def user_update
    if params[:user][:password] == "" || params[:user][:password] == ""
      flash[:notice] = ["Password can't be blank"]
      redirect_to '/profile'
    elsif params[:user][:password] == params[:user][:re_password] 
      user = User.find_by(:id => session[:user_id])
      user.update_attributes!(:name => params[:user][:name], 
                             :email => params[:user][:email], 
                             :password => params[:user][:password])
      flash[:notice] = ["Profile is updated"] 
      redirect_to '/profile'
    else
      flash[:notice] = ["Passwords are mismatch"]
      redirect_to '/profile'
    end
  rescue ActiveRecord::RecordInvalid => e
   flash[:notice] = e.to_s.sub(/^[a-zA-Z0-9\s]+: /, "").split(", ")
    redirect_to '/profile'
  end

  def new
    errors = {"Name can't be blank" => "名前の欄を入力してください",
              "Email can't be blank" => "メールアドレスの欄を入力して下さい" ,
              "Email is invalid" => "メールアドレスの形が不正です",
              "Password can't be blank" => "パスワードの欄を入力してください",
              "Password is too short (minimum is 6 characters)" => "パスワードは6文字以上で入力して下さい",
              "This Email address is already signed up" => "このメールアドレスはすでに登録されています"}

    if flash[:notice] != nil
      flash[:notice].each do |msg|
        msg.sub!(msg, errors[msg])
      end
    end

    if flash[:notice] == nil
      flash[:notice] = ""
    end
  rescue => e
    flash[:notice].clear
    redirect_to '/new'
  end

  def login
    if flash[:login] == nil
      flash[:login] = ""
    end
  end

  def logout
    session[:user_id] = nil
    redirect_to '/login'
  end

  def login_process
    user = User.find_by(:email => params[:user][:email])
    if user == nil
      flash[:login] = ["メールアドレスまたはパスワードが違います"]
      redirect_to '/login'
    else
      if user.authenticate(params[:user][:password])
        session[:user_id] = user[:id]
        redirect_to '/dashboard'
      else
        flash[:login] = ["メールアドレスまたはパスワードが違います"]
        redirect_to '/login'
      end
    end
  end

  def user_create
    @users = User.find_by(:email => params[:user][:email])
    if @users == nil
      if user = User.create!(
        :name => params[:user][:name],
        :email => params[:user][:email],
        :password => params[:user][:password]
        )
        session[:user_id] = user.id
        redirect_to '/'
      else
        flash[:notice] = ["Failed to creating user"]
        redirect_to '/new'
      end
    else
      flash[:notice] = ["This Email address is already signed up"]
      redirect_to '/new'
    end
  rescue ActiveRecord::RecordInvalid => e
    flash[:notice] = e.to_s.sub(/^[a-zA-Z0-9\s]+: /, "").split(", ")
    redirect_to '/new'
  end
end
