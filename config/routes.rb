Rails.application.routes.draw do

  root 'main#judge' 

  get '/login' => "main#login"
  get '/logout' => "main#logout"
  post '/login' => 'main#login_process'
  get '/new' => 'main#new'
  post '/new' => 'main#user_create'
  get '/profile' => 'main#profile'
  post '/profile' => 'main#user_update'

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  namespace :api, {format: 'json'} do
    resources :categories, only: [:index, :show] do
      resources :ingredients, only: [:index, :show]
    end
    resources :ingredients, only: [:index, :show]
    resources :fridges, except: [:new, :edit]
    resources :recipes, only: [:index, :show]
    get '/suggests' => 'suggests#get'
  end
end
